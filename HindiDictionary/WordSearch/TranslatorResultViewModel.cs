using System.Collections.ObjectModel;
using Caliburn.Micro;
using HindiDictionary.SearchEngine;

namespace HindiDictionary.WordSearch
{
    public class BeginExecuteCommand
    {
    }

    public class TranslatorResultViewModel : Screen
    {
        
        private ObservableCollection<Meaning> searchResult;

        
        public ObservableCollection<Meaning> SearchResult
        {
            get { return searchResult; }
            set
            {
                if (Equals(value, searchResult)) return;
                searchResult = value;
                NotifyOfPropertyChange();
            }
        }
        
    }



}