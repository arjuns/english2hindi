﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;
using Caliburn.Micro;
using FontAwesome.WPF;
using HindiDictionary.WordSearch;

namespace HindiDictionary.Controls
{
    public class MeaningSearchAction : TriggerAction<Button>
    {
        public static readonly DependencyProperty TargetProperty =
                DependencyProperty.Register("Target", typeof(TextBox), typeof(MeaningSearchAction), new UIPropertyMetadata(null));


        public TextBox Target
        {
            get { return (TextBox)GetValue(TargetProperty); }
            set { SetValue(TargetProperty, value); }
        }


        protected override void OnAttached()
        {
            base.OnAttached();
            this.Target.PreviewKeyDown += KeyDown;
        }

        private void KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                DoSearch();
            }
        }

        private async void DoSearch()
        {
            await IoC.Get<IEventAggregator>().PublishOnUIThreadAsync(new BeginExecuteCommand());
        }

        protected override void OnDetaching()
        {
            this.AssociatedObject.PreviewKeyDown -= KeyDown;
            base.OnDetaching();
        }

        protected override void Invoke(object parameter)
        {
            DoSearch();
        }
    }
}