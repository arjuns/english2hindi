using System.Linq;
using HindiDictionary.Shell;
using SimpleInjector;

namespace HindiDictionary
{
    using System;
    using System.Collections.Generic;
    using Caliburn.Micro;

    public class AppBootstrapper : BootstrapperBase
    {
        readonly Container container = new Container();

        public AppBootstrapper()
        {
            Initialize();
        }

        protected override void Configure()
        {
            //container = new SimpleContainer();

            CompositionRoot.Initialize(container);
           
        }

        protected override object GetInstance(Type service, string key)
        {
            var instance = container.GetInstance(service);
            if (instance != null)
                return instance;

            throw new InvalidOperationException("Could not locate any instances.");
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
          //return container.GetAllInstances(service);
            return Enumerable.Empty<object>();
        }

        protected override void BuildUp(object instance)
        {
            container.Verify();
        }

        protected override void OnStartup(object sender, System.Windows.StartupEventArgs e)
        {

            DisplayRootViewFor<ShellViewModel>();
        }
    }
}