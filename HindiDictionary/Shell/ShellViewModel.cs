using Caliburn.Micro;
using HindiDictionary.WordSearch;

namespace HindiDictionary.Shell
{
    public class ShellViewModel : Conductor<Screen>.Collection.OneActive, IHandle<BeginExecuteCommand>
    {
       
        private readonly IEventAggregator evt;
        private readonly CommandNavigatorRegistry registry;
        public ShellViewModel(IEventAggregator evt)
        {

            this.evt = evt;
            evt.Subscribe(this);
            registry = new CommandNavigatorRegistry();

        }


        public string Word { get; set; }



        public async void Handle(BeginExecuteCommand message)
        {
           await this.registry.Navigate(this.Word);

        }
        
    }
}