using System.Threading.Tasks;
using HindiDictionary.Empty;
using HindiDictionary.Options;

namespace HindiDictionary.Shell
{
    public class OptionsNavigator : ICommandNavigator
    {
        private readonly OptionViewModel emptyView;
        private readonly ShellViewModel shell;

        public OptionsNavigator(OptionViewModel emptyView, ShellViewModel shell)
        {
            this.emptyView = emptyView;
            this.shell = shell;
        }

        public Task NavigateAsync(string command)
        {
            this.shell.ActivateItem(emptyView);
            return Task.FromResult(0);
        }
    }
}