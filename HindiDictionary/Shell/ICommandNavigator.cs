using System.Threading.Tasks;

namespace HindiDictionary.Shell
{
    public interface ICommandNavigator
    {
        Task NavigateAsync(string command);
    }
}