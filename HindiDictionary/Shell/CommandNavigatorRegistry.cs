using System.Threading.Tasks;
using Caliburn.Micro;
using HindiDictionary.Options;

namespace HindiDictionary.Shell
{
    public class CommandNavigatorRegistry
    {
        private string lastCommand = "";
        public async Task Navigate(string word)
        {
            if (word == lastCommand)
                return;
            if (word.StartsWith("?"))
            {
                await IoC.Get<OptionsNavigator>().NavigateAsync(word);
            }
            else
            {
                await IoC.Get<FindMeaningNavigator>().NavigateAsync(word);
            }
        }
    }
}