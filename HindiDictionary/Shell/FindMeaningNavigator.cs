using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Caliburn.Micro;
using HindiDictionary.Loader;
using HindiDictionary.Queries;
using HindiDictionary.SearchEngine;
using HindiDictionary.WordSearch;

namespace HindiDictionary.Shell
{
    public class FindMeaningNavigator : ICommandNavigator
    {
        private readonly IMediator mediator;
        private readonly ShellViewModel shell;
        private readonly ILoader loader;

        public FindMeaningNavigator(IMediator mediator, ShellViewModel shell, ILoader loader)
        {
            this.mediator = mediator;
            this.shell = shell;
            this.loader = loader;
        }

        public async Task NavigateAsync(string command)
        {
            loader.Show();
            var enumerable = await mediator.Execute(new TranslateWordCommand() { Word = command });
            var vm = IoC.Get<TranslatorResultViewModel>();
            vm.SearchResult = new ObservableCollection<Meaning>(enumerable);
            shell.ActivateItem(vm);
            loader.Hide();
        }
    }
}