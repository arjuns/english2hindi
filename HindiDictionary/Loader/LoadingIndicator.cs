using System;
using System.Windows;
using System.Windows.Media;
using FontAwesome.WPF;

namespace HindiDictionary.Loader
{

    public interface ILoader
    {
        void Hide();
        void Show(string message = "loading");
    }

    public class LoadingIndicator : ILoader
    {

        private readonly Lazy<ImageAwesome> LoaderIcon = new Lazy<ImageAwesome>(() => FindChild<ImageAwesome>(Application.Current.MainWindow, "SpinningImage"));

        void ILoader.Hide()
        {
            if (LoaderIcon.Value != null)
            {
                LoaderIcon.Value.Icon = FontAwesomeIcon.Search;
                LoaderIcon.Value.Spin = false;
            }

        }

        void ILoader.Show(string msg)
        {

            if (LoaderIcon.Value != null)
            {
                LoaderIcon.Value.Icon = FontAwesomeIcon.Spinner;
                LoaderIcon.Value.Spin = true;
            }


        }

        public static T FindChild<T>(DependencyObject parent, string childName) where T : DependencyObject
        {
            if (parent == null) return null;

            T foundChild = null;

            var childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (var i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                var childType = child as T;
                if (childType == null)
                {
                    foundChild = FindChild<T>(child, childName);
                    if (foundChild != null) break;
                }
                else if (!string.IsNullOrEmpty(childName))
                {
                    var frameworkElement = child as FrameworkElement;
                    if (frameworkElement != null && frameworkElement.Name == childName)
                    {
                        foundChild = (T)child;
                        break;
                    }
                }
                else
                {
                    foundChild = (T)child;
                    break;
                }
            }

            return foundChild;
        }

    }


}