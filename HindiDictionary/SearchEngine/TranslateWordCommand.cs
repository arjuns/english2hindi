﻿using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CsQuery;
using HindiDictionary.Queries;

namespace HindiDictionary.SearchEngine
{

    public class TranslateWordCommand : ICommand<IEnumerable<Meaning>>
    {
        public string Word { get; set; }
    }

    public class TranslateWordCommandHandler : ICommandHandler<TranslateWordCommand, IEnumerable<Meaning>>
    {

        public async Task<string> DownloadDom(string text)
        {
            var url = $"http://www.shabdkosh.com/hi/translate?e={text}";
            WebClient client = new WebClient { Encoding = Encoding.UTF8 };
            return await client.DownloadStringTaskAsync(url).ConfigureAwait(false);
        }

        public async Task<IEnumerable<Meaning>> GetMeaning(string text)
        {
            List<Meaning> items = new List<Meaning>();
            var s = await DownloadDom(text);
            var cq = new CQ(s);
            items.AddRange(GetMeaningWithType("noun", cq));
            items.AddRange(GetMeaningWithType("adjective", cq));
            items.AddRange(GetMeaningWithType("verb", cq));

            return items;
        }

        private IEnumerable<Meaning> GetMeaningWithType(string type, CQ cq)
        {
            var first = cq[$"h3.{type}:first-of-type + ol.eirol"].First();
            foreach (var child in first.Children())
            {
                var q = CQ.Create(child);
                var parsed = ParseSingle(q);
                parsed.Type = type;
                yield return parsed;
            }
        }


        private Meaning ParseSingle(CQ fragment)
        {
            var meaning = new Meaning();
            meaning.Title = fragment["a"].Text();
            return meaning;
        }

        public Task<IEnumerable<Meaning>> Handle(TranslateWordCommand item)
        {
            return GetMeaning(item.Word);

        }
    }
}