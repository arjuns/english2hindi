﻿namespace HindiDictionary.SearchEngine
{
    public class Meaning
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
    }
}