using System;

namespace HindiDictionary.Queries
{
    public interface ICommand
    {
        
    }
    public interface IQuery<out T>
    {
    }

    public interface ICommand<out T>:ICommand
    {
    }
    public interface IPageableQuery
    {
       
        int PageIndex { get; set; }
    }
    [AttributeUsage(AttributeTargets.Property,AllowMultiple = false)]
    public class QueryString : Attribute
    {
        public string Name { get; private set; }
        public QueryString(string name)
        {
            this.Name = name;
        }

    }
}