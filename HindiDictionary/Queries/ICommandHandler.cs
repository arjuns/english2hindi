using System.Threading.Tasks;

namespace HindiDictionary.Queries
{
    public interface ICommandHandler<in TRequest, TResponse> where TRequest : ICommand<TResponse>
    {
        Task<TResponse> Handle(TRequest item);

    }
}