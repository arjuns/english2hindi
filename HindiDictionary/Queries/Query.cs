using System.Threading.Tasks;

namespace HindiDictionary.Queries
{
    public interface IQueryHandler<in TQuery, TResponse>
        where TQuery : IQuery<TResponse>
    {
        Task<TResponse> Handle(TQuery request);
    }
}