using System.Reflection;
using System.Threading.Tasks;
using SimpleInjector;

namespace HindiDictionary.Queries
{
    public class Mediator : IMediator
    {
        readonly Container container;

        public Mediator(Container  container)
        {
            this.container = container;
        }

        public virtual Task<TResponseData> Query<TResponseData>(IQuery<TResponseData> query)
        {
            var handler = GetQueryHandler(query);
            return ProcessQueryWithHandler(query, handler);
        }
       

        public Task<TResponse> Execute<TResponse>(ICommand<TResponse> form)
        {
            var handler = GetFormHandler(form);
            return ProcessFormWithHandler(form, handler);
        }
       

        static Task<TResponseData> ProcessQueryWithHandler<TResponseData>(IQuery<TResponseData> query, object handler)
        {

            return (Task<TResponseData>)GetHandlerMethod(handler, query, "Handle").Invoke(handler, new object[] { query });
        }


        static Task<TResponseData> ProcessFormWithHandler<TResponseData>(ICommand<TResponseData> form,
            object handler)
        {

            return (Task<TResponseData>)GetHandlerMethod(handler, form, "Handle").Invoke(handler, new object[] { form });

        }
        

        static MethodInfo GetHandlerMethod(object handler, object query, string name)
        {
            return handler.GetType()
                .GetMethod(name,
                    BindingFlags.Public | BindingFlags.Instance | BindingFlags.InvokeMethod,
                    null, CallingConventions.HasThis,
                    new[] { query.GetType() },
                    null);
        }

        object GetQueryHandler<TResponseData>(IQuery<TResponseData> query)
        {
            var handlerType = typeof(IQueryHandler<,>).MakeGenericType(query.GetType(), typeof(TResponseData));
            var handler = container.GetInstance(handlerType);
            return handler;
        }

        object GetFormHandler<TResponseData>(ICommand<TResponseData> query)
        {
            var handlerType = typeof(ICommandHandler<,>).MakeGenericType(query.GetType(), typeof(TResponseData));
            var handler = container.GetInstance(handlerType);
            return handler;
        }
    }


}