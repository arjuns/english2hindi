using System.Threading.Tasks;

namespace HindiDictionary.Queries
{
    public interface IMediator
    {
        Task<TResponse> Query<TResponse>(IQuery<TResponse> query);
        Task<TResponse> Execute<TResponse>(ICommand<TResponse> form);
        
    }
}