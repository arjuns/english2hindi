using System;
using System.Globalization;
using System.Windows.Data;
using Caliburn.Micro;
using HindiDictionary.SearchEngine;
using HindiDictionary.WordSearch;

namespace HindiDictionary.Assets
{
    public class MeaningTypeValueConverter:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                if (value.ToString() == "noun")
                    return "noun";
                if (value.ToString() == "adjective")
                    return "adj.";
                if (value.ToString() == "verb")
                    return "verb";
            }
            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
        
    }
}