using System;
using System.Reflection;
using Caliburn.Micro;
using HindiDictionary.Empty;
using HindiDictionary.Loader;
using HindiDictionary.Options;
using HindiDictionary.Queries;
using HindiDictionary.Shell;
using HindiDictionary.WordSearch;
using SimpleInjector;
using SimpleInjector.Advanced;

namespace HindiDictionary
{
    public class InjectAttribute : Attribute
    {

    }
    public class CompositionRoot
    {
        public static void Initialize(Container container)
        {
            container.Options.PropertySelectionBehavior = new PropertySelectionBehivior();
            container.RegisterSingleton<IWindowManager, WindowManager>();
            container.RegisterSingleton<IEventAggregator, EventAggregator>();
            container.RegisterSingleton<IMediator, Mediator>();
            container.Register<ILoader, LoadingIndicator>();

            container.RegisterSingleton<ShellViewModel>(); //shell view always single tone
            container.Register<TranslatorResultViewModel>();
            container.Register<OptionViewModel>();
            container.Register<EmptyResultViewModel>();
            


            container.Register<FindMeaningNavigator>();


            container.Register(typeof(IQueryHandler<,>), new[] { Assembly.GetExecutingAssembly() });
            container.Register(typeof(ICommandHandler<,>), new[] { Assembly.GetExecutingAssembly() });
        }
    }


    public class PropertySelectionBehivior : IPropertySelectionBehavior
    {
        public bool SelectProperty(Type serviceType, PropertyInfo propertyInfo)
        {
            return propertyInfo.IsDefined(typeof(InjectAttribute));
        }
    }
}